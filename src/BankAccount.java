package src;

public class BankAccount {

    private double balance;
    static int ID=0;

    public BankAccount(double balance){
        this.balance = balance;
        this.ID ++;
    }


    public double getBalance() {
        return balance;
    }

    public void creditBalance(double amount) throws Exception {
        if(amount > 0)
            this.balance += amount;

        else
            throw new IllegalArgumentException("Montant négatif");
    }

    public void debitBalance(double amount) throws Exception {
        if(amount < 0 ) throw new IllegalArgumentException("Montant négatif"); // Thrown si le montant est négatif

        else if (this.balance-amount <= 0) throw new IllegalArgumentException("Cause un découvert"); // Thrown si le montant cause un découvert

                else if (amount >= this.balance) throw new IllegalArgumentException("Montant à débiter inférieur au solde"); // si montant < solde

                    else this.balance -= amount;  // l'opération est effectuée


    }

    public void transferAmount(BankAccount account, double amount) throws Exception {
        if(account != null) {
            this.debitBalance(amount);
            account.creditBalance(amount);
        }
        else throw new InexistantBankAccountException ("Compte null");

    }
}
