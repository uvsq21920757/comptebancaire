package src;

public class InexistantBankAccountException extends Exception {

    InexistantBankAccountException(String msg){
        super(msg);
    }
}
