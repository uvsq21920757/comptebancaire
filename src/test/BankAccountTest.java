import org.junit.Test;
import src.BankAccount;
import src.InexistantBankAccountException;

public class BankAccountTest {

    @Test (expected = IllegalArgumentException.class)
    public void testPositiveAmmount() throws Exception { //test que le montant soit positif

        BankAccount testAcc = new BankAccount(5000);

        testAcc.creditBalance(-500);

    }

    @Test (expected = IllegalArgumentException.class)
    public void testAccountOverdraft() throws Exception{ //test le découvert

        BankAccount testAcc = new BankAccount(5000);

        testAcc.debitBalance(5500);
    }

    @Test(expected = InexistantBankAccountException.class)
    public void testAccountExistance() throws Exception {

        BankAccount testAcc = new BankAccount(5000);
        BankAccount testAcc2 = null;

        testAcc.transferAmount(testAcc2, 500);

    }

}
